#!/bin/bash
export ITK_PATH=$(readlink -f $(dirname ${BASH_SOURCE[0]}))
export ITK_INST_PATH=${ITK_PATH}/installed

#setup TDAQ
export TDAQ_VERSION=tdaq-11-02-01

TDAQPATHS=(/home/atlas /cvmfs/atlas.cern.ch/repo/sw/tdaq)
for _cand in ${TDAQPATHS[@]}; do
    if [ -d ${_cand} ]; then
        export TDAQ_RELEASE_BASE=${_cand}
        export CMAKE_PROJECT_PATH=${_cand}
	export TOOL_BASE=${_cand}/tools/x86_64-el9
        break
    fi
done
LCGPATHS=(/home/atlas/sw/lcg/releases /cvmfs/sft.cern.ch/lcg/releases)
for _cand in ${LCGPATHS[@]}; do
    if [ -d ${_cand} ]; then
        export LCG_RELEASE_BASE=${_cand}
        export LCG_INST_PATH=${_cand}
        break
    fi
done
if [ -z ${TDAQ_RELEASE_BASE+x} ]; then echo "TDAQ release base not found"; return 0; fi

echo "---------"
echo "TDAQ_RELEASE_BASE=${TDAQ_RELEASE_BASE}"
echo "LCG_RELEASE_BASE=${LCG_RELEASE_BASE}"
echo "CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH}"
source $TDAQ_RELEASE_BASE/tdaq/$TDAQ_VERSION/installed/setup.sh 
source $TDAQC_INST_PATH/share/cmake_tdaq/bin/setup.sh
echo "---------"

#setup ITK SW
export PATH=$ITK_INST_PATH/$CMTCONFIG/bin:$ITK_INST_PATH/share/bin:$PATH
export LD_LIBRARY_PATH=$ITK_INST_PATH/$CMTCONFIG/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$ITK_INST_PATH/external/$CMTCONFIG/lib:$LD_LIBRARY_PATH

#setup Python
export PATH=$TDAQ_PYTHON_HOME/bin:$PATH
export PYTHONPATH=$ITK_INST_PATH/share/lib/python:$PYTHONPATH
export PYTHONPATH=$ITK_INST_PATH/$CMTCONFIG/lib:$PYTHONPATH
export PYTHONPATH=$ITK_INST_PATH/share/bin:$PYTHONPATH

#setup GDB
export PYTHONHOME=$(dirname $(dirname $(which python)))
export PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/gdb/13.2/${CMTCONFIG}/bin:$PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/gdb/13.2/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
 
#setup Root
export PATH=$ROOTSYS/bin:$PATH
export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH

#missing libraries
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/glib/2.76.2/${CMTCONFIG}/lib64/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/pango/1.48.9/${CMTCONFIG}/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/harfbuzz/2.7.4/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/cairo/1.17.2/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/freetype/2.10.0/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/blas/0.3.20.openblas/${CMTCONFIG}/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/uproot/4.3.7/${CMTCONFIG}/lib:$LD_LIBRARY_PATH

#doxygen
export PATH=${LCG_RELEASE_BASE}/${TDAQ_LCG_RELEASE}/doxygen/1.8.18/${CMTCONFIG}/bin:$PATH

#quasar specific
export XERCESC_ROOT=${LCG_RELEASE_PATH}/${TDAQ_LCG_RELEASE}/XercesC/3.1.3/${CMTCONFIG}
export BOOST_ROOT=${LCG_RELEASE_PATH}/${TDAQ_LCG_RELEASE}/Boost/1.72.0/${CMTCONFIG} 

#alias
alias COMPILE='cd $ITK_PATH/build;make install -j;cd $ITK_PATH'

